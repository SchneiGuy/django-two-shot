from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateForm, CategoryForm, AccountForm

# Create your views here.


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)

    context = {
        "account_list": account,
    }

    return render(request, "receipts/account_list.html", context)


@login_required
def categories_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "expense_list": expenses,
    }

    return render(request, "receipts/categories.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            name = form.save(False)
            name.owner = request.user
            name.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            name = form.save(False)
            name.owner = request.user
            name.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_account.html", context)
